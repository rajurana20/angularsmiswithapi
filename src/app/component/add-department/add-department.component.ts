import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';

import {DepartmentService} from '../../service/department.service';
import {Department} from '../../model/department';

@Component({
  selector: 'app-add-department',
  templateUrl: './add-department.component.html',
  styleUrls: ['./add-department.component.css']
})
export class AddDepartmentComponent implements OnInit {
  isSuccess = false;
  showDiv = false;
  createDepartmentForm: FormGroup;
  constructor( private fb: FormBuilder, private departmentService: DepartmentService) { }
  private buildForm()
  {
    this.createDepartmentForm = this.fb.group({
        departmentName: ['', Validators.required],
        description : ['']
      }
    );
  }
  get departmentName() { return this.createDepartmentForm.get('departmentName'); }

  ngOnInit(): void {
    this.buildForm();
  }

  saveDepartment() {
    this.departmentService.saveDepartment(this.createDepartmentForm.value).subscribe(
      (response: Department) => { this.isSuccess = true; this.showDiv = true; this.buildForm(); },
        error => { console.error(error); }
    );
  }

  closeDiv() {
    this.showDiv = false;
  }
}
