import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {StudentService} from '../../service/student.service';
import {DepartmentService} from '../../service/department.service';
import {Student} from '../../model/student';
import {Department} from '../../model/department';

@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.css']
})
export class AddStudentComponent implements OnInit {
  isSuccess = false;
  showDiv = false;
  createStudentForm: FormGroup;
  departmentList: Array<Department> = new Array<Department>();
  constructor( private fb: FormBuilder, private studentService: StudentService, private departmentService: DepartmentService) { }
  private buildForm()
  {
    this.createStudentForm = this.fb.group({
        studentName: ['', Validators.required],
        address: ['', Validators.required],
        departmentId : ['']
      }
    );
  }
  get studentName() { return this.createStudentForm.get('studentName'); }
  get address() { return this.createStudentForm.get('address'); }

  ngOnInit(): void {
    this.buildForm();
    this.getAllDepartments();
  }
  getAllDepartments()
  {
    this.departmentService.getAll().subscribe(
      (resonse: any) => {this.departmentList = resonse; }
    );
  }

  saveStudent() {
    console.log(this.createStudentForm.value);
    this.studentService.saveStudent(this.createStudentForm.value).subscribe(
      (response: Student) => { this.isSuccess = true; this.showDiv = true; this.buildForm(); },
      error => { console.error(error); }
    );
  }

  closeDiv() {
    this.showDiv = false;
  }
}
