import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import {Student} from '../../model/student';
import {StudentService} from '../../service/student.service';

@Component({
  selector: 'app-list-student',
  templateUrl: './list-student.component.html',
  styleUrls: ['./list-student.component.css']
})
export class ListStudentComponent implements OnInit {
  studentList: Array<Student> = new Array<Student>();
  constructor(private studentService: StudentService ) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(): void
  {
    this.studentService.listStudent().subscribe(
      (response: any) => { this.studentList = response; }, (error: any) => {console.error(error); }
    );
  }
  deleteStudent(studentId: any): void {
    this.studentService.deleteStudent(studentId).subscribe(
      (response: any ) => {this.getAll(); }, (error) => {alert(error); }
    );
  }
}
