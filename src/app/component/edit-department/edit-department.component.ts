import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

import {DepartmentService} from '../../service/department.service';
import {Department} from '../../model/department';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-edit-department',
  templateUrl: './edit-department.component.html',
  styleUrls: ['./edit-department.component.css']
})
export class EditDepartmentComponent implements OnInit {
  id: number;
  department: Department;
  isSuccess = false;
  showDiv = false;
  updateDepartmentForm: FormGroup;
  constructor( private fb: FormBuilder, private departmentService: DepartmentService, private router: ActivatedRoute) { }
  private buildForm()
  {
    this.updateDepartmentForm = this.fb.group({
        departmentId: [''],
        departmentName: ['', Validators.required],
        description : ['']
      }
    );
  }
  get departmentName() { return this.updateDepartmentForm.get('departmentName'); }

  ngOnInit(): void {
    this.id = this.router.snapshot.params.id;
    this.getEmployeeById(this.id);
    this.buildForm();
  }

  private getEmployeeById(id: number): void
  {
    this.departmentService.getById(id).subscribe(
      (response: Department) =>
      {
        this.updateDepartmentForm = this.fb.group({
          departmentId: [id],
          departmentName: [response.departmentName, Validators.required],
          description : [response.description]
        });
        this.department = response;
      }
    );
  }

  saveDepartment() {
    this.departmentService.saveDepartment(this.updateDepartmentForm.value).subscribe(
      (response: Department) => { this.isSuccess = true; this.showDiv = true; this.buildForm(); },
      error => { console.error(error); }
    );
  }
  updateDepartment() {
    debugger;
    this.departmentService.updateDepartment(this.updateDepartmentForm.value).subscribe(
      (response: Department) => { this.isSuccess = true; this.showDiv = true; this.buildForm(); },
      (error: any) => { console.error(error); }
    );
  }
  closeDiv() {
    this.showDiv = false;
  }
}
