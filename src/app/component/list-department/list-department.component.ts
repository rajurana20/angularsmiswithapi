import { Component, OnInit } from '@angular/core';
import {Department} from '../../model/department';
import {DepartmentService} from '../../service/department.service';

@Component({
  selector: 'app-list-department',
  templateUrl: './list-department.component.html',
  styleUrls: ['./list-department.component.css']
})
export class ListDepartmentComponent implements OnInit {
  departments: Array<Department> = new Array<Department>();
  constructor(private departmentService: DepartmentService) { }

  ngOnInit(): void {
    this.getAll();
  }
  getAll(): void{
    this.departmentService.getAll().subscribe(
      (response: any) => {this.departments = response; }, error => {console.error(error); }
    );
  }
  deleteDepartment(departmentId: number): void {
    this.departments.splice(departmentId - 1, 1);
    this.departmentService.deleteDepartment(departmentId).subscribe(
      (response: any) => { this.getAll(); }
    );
    this.getAll();
  }
}
