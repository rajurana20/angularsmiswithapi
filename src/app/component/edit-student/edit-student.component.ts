import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';

import {Department} from '../../model/department';
import {StudentService} from '../../service/student.service';
import {DepartmentService} from '../../service/department.service';
import {Student} from '../../model/student';

@Component({
  selector: 'app-edit-student',
  templateUrl: './edit-student.component.html',
  styleUrls: ['./edit-student.component.css']
})
export class EditStudentComponent implements OnInit {
  isSuccess = false;
  showDiv = false;
  student: Student;
  updateStudentForm: FormGroup;
  departmentList: Array<Department> = new Array<Department>();
  constructor( private fb: FormBuilder, private studentService: StudentService, private departmentService: DepartmentService,
               private router: ActivatedRoute) { }
  private buildForm()
  {
    this.updateStudentForm = this.fb.group({
        studentId : [''],
        studentName: ['', Validators.required],
        address: ['', Validators.required],
        departmentId : ['']
      }
    );
  }
  get studentName() { return this.updateStudentForm.get('studentName'); }
  get address() { return this.updateStudentForm.get('address'); }

  ngOnInit(): void {
    this.getAllDepartments();
    this.getStudentById(this.router.snapshot.params.id);
  }
  getAllDepartments()
  {
    this.departmentService.getAll().subscribe(
      (resonse: any) => {this.departmentList = resonse; }
    );
  }
  private getStudentById(id: number): void
  {
    this.studentService.getStudentById(id).subscribe(
      (resonse: any) => {this.student = resonse; this.updateStudentForm = this.fb.group({
        studentId : [id],
        studentName: [resonse.studentName, Validators.required],
        address: [resonse.address, Validators.required],
        departmentId : [resonse.department.departmentId]
      }); }
    );
  }
  updateStudent() {
    console.log(this.updateStudentForm.value);
    this.studentService.updateStudent(this.updateStudentForm.value).subscribe(
      (response: Student) => { this.isSuccess = true; this.showDiv = true; this.buildForm(); },
      error => { console.error(error); }
    );
  }

  closeDiv() {
    this.showDiv = false;
  }
}
